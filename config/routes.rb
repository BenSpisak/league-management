Rails.application.routes.draw do
  resources :games
  resources :teams do
    member do
      get 'detach'
      get 'makeOwner'
      get 'removeOwner'
    end
  end
  resources :leagues
  devise_for :users, :controllers => {:registrations => "registrations", confirmations: 'confirmations'}
  root to: 'pages#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  devise_scope :user do
    get 'login', to: 'devise/sessions#new'
  end

  devise_scope :user do
    get 'signup', to: 'devise/registrations#new'
  end

  mount LetterOpenerWeb::Engine, at: '/letter_opener' if Rails.env.development?
end
