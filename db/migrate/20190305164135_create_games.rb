class CreateGames < ActiveRecord::Migration[5.2]
  def change
    create_table :games do |t|
      t.datetime :start
      t.datetime :end
      t.integer :home_score, default: 0
      t.integer :away_score, default: 0
      t.string :field
      t.references :home_team
      t.references :away_team

      t.timestamps
    end
  end
end
