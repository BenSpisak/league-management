class AddImageToLeagues < ActiveRecord::Migration[5.2]
  def change
    add_column :leagues, :image, :string
  end
end
