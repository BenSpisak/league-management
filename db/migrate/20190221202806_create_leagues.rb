class CreateLeagues < ActiveRecord::Migration[5.2]
  def change
    create_table :leagues do |t|
      t.references :user
      t.string :name
      t.string :location

      t.timestamps
    end
  end
end
