class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :team
      t.string :kind
      t.string :name
      t.integer :minute
      t.string :reason
      t.references :game, foreign_key: true

      t.timestamps
    end
  end
end
