
User.create!([
  {email: "user1@gmail.com", password: "Password1", name: "I am the league owner", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, image: nil, confirmation_token: "1cqUgU4HufwPss2h6M8X", confirmed_at: "2019-03-17 15:33:02", confirmation_sent_at: "2019-03-17 15:33:00"},
  {email: "benjaminspisak@gmail.com", password: "Password1", name: "Ben", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, image: nil, confirmation_token: "h2ba4xkWPP7iSpPtx-zt", confirmed_at: "2019-03-17 17:24:06", confirmation_sent_at: "2019-03-17 15:31:03"},
  {email: "user2@gmail.com", password: "Password1", name: "Joeseph ManDude", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, image: "BigBallerBrand.jpg", confirmation_token: "R23FY9EC3NC2h1eNxyjc", confirmed_at: "2019-03-17 15:33:46", confirmation_sent_at: "2019-03-17 15:33:42"},
  {email: "user3@gmail.com", password: "Password1", name: "Henry Suave", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, image: nil, confirmation_token: "q3KxNom2hTnj8DRg_5yo", confirmed_at: "2019-03-19 21:21:52", confirmation_sent_at: "2019-03-19 21:21:49"},
  {email: "user4@gmail.com", password: "Password1", name: "Harry Smith", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, image: nil, confirmation_token: "zEwkYWru_yUUNHwwWfjs", confirmed_at: "2019-03-19 21:22:46", confirmation_sent_at: "2019-03-19 21:22:42"},
  {email: "user5@gmail.com", password: "Password1", name: "Smith Butterson", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, image: nil, confirmation_token: "eKfkKtRvU_L75YGN_DrX", confirmed_at: "2019-03-19 21:23:19", confirmation_sent_at: "2019-03-19 21:23:16"},
  {email: "user6@gmail.com", password: "Password1", name: "Smithsonian Museum", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, image: nil, confirmation_token: "iQE6mv99Va1zhy_68joM", confirmed_at: "2019-03-19 21:23:47", confirmation_sent_at: "2019-03-19 21:23:45"},
  {email: "user7@gmail.com", password: "Password1", name: "Sara Burger", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, image: nil, confirmation_token: "LAZtjyjF9EDFsa9rvBpz", confirmed_at: "2019-03-19 21:24:27", confirmation_sent_at: "2019-03-19 21:24:23"},
  {email: "user8@gmail.com", password: "Password1", name: "Hailee Smithington", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, image: nil, confirmation_token: "73NtANEAHJZvz4f_UHTh", confirmed_at: "2019-03-19 21:25:04", confirmation_sent_at: "2019-03-19 21:25:01"},
  {email: "user9@gmail.com", password: "Password1", name: "Lacey Harris", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, image: nil, confirmation_token: "us7yZwK1ebQoi3fWyTob", confirmed_at: "2019-03-19 21:25:55", confirmation_sent_at: "2019-03-19 21:25:53"},
  {email: "user11@gmail.com", password: "Password1", name: "Super Frankington", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, image: nil, confirmation_token: "y7x-yNoy3Nxypux3T3pN", confirmed_at: "2019-03-19 21:26:53", confirmation_sent_at: "2019-03-19 21:26:50"},
  {email: "user12@gmail.com", password: "Password1", name: "Felipe Anderson", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, image: nil, confirmation_token: "ghex3oyXfx6xtsYfKY6i", confirmed_at: "2019-03-19 21:27:39", confirmation_sent_at: "2019-03-19 21:27:36"},
  {email: "user13@gmail.com", password: "Password1", name: "Joe Jonas", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, image: nil, confirmation_token: "j2fbomqRkRzVpncdFYkm", confirmed_at: "2019-03-19 21:28:13", confirmation_sent_at: "2019-03-19 21:28:09"},
  {email: "user14@gmail.com", password: "Password1", name: "Peter Parker", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, image: nil, confirmation_token: "zgNspDD6_W4WhXQByMMx", confirmed_at: "2019-03-19 21:28:52", confirmation_sent_at: "2019-03-19 21:28:49"},
  {email: "user15@gmail.com", password: "Password1", name: "Alex Kavington", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, image: nil, confirmation_token: "WCVG7iRrcNHUCTyQoJEy", confirmed_at: "2019-03-19 21:29:28", confirmation_sent_at: "2019-03-19 21:29:25"},
  {email: "user16@gmail.com", password: "Password1", name: "Darth Vader", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, image: nil, confirmation_token: "PJETZKYx4wfFVBs14f5z", confirmed_at: "2019-03-19 21:30:06", confirmation_sent_at: "2019-03-19 21:30:03"},
  {email: "user10@gmail.com", password: "Password1", name: "Thanos NiceGuy", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, image: nil, confirmation_token: "BbGVhZcQVcDFeeuZshfU", confirmed_at: "2019-03-19 21:35:20", confirmation_sent_at: "2019-03-19 21:35:17"},
  {email: "user1000@gmail.com", password: "Password1", name: "Hey I own a league too!!!", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, image: nil, confirmation_token: "9z9JZcs5CLWgGyNGFZfz", confirmed_at: "2019-03-20 00:26:39", confirmation_sent_at: "2019-03-20 00:26:36"}
])
League.create!([
  {user_id: 2, name: "Big Ballers League", location: "Sanfranscico (Address)", image: "BigBallerBrand.jpg"},
  {user_id: 18, name: "Jiust another league", location: "This place", image: nil}
])
Team.create!([
  {name: "Keep it 1hunnid", wins: 1, losses: 0, league_id: 1, image: "onehunnid.png"},
  {name: "Biggest Ballers", wins: 2, losses: 1, league_id: 1, image: "bmoji.png"},
  {name: "Hey we are here too :)", wins: 0, losses: 1, league_id: 1, image: nil},
  {name: "Tiny Ballers", wins: 0, losses: 1, league_id: 1, image: "smallerball.png"},
  {name: "Team1", wins: 0, losses: 0, league_id: 2, image: nil},
  {name: "Team2", wins: 0, losses: 0, league_id: 2, image: nil},
  {name: "Team3", wins: 0, losses: 0, league_id: 2, image: nil}
])

Membership.create!([
  {user_id: 3, team_id: 1},
  {user_id: 4, team_id: 1},
  {user_id: 5, team_id: 1},
  {user_id: 1, team_id: 1},
  {user_id: 6, team_id: 4},
  {user_id: 7, team_id: 4},
  {user_id: 8, team_id: 4},
  {user_id: 9, team_id: 4},
  {user_id: 10, team_id: 2},
  {user_id: 11, team_id: 2},
  {user_id: 12, team_id: 2},
  {user_id: 13, team_id: 2},
  {user_id: 14, team_id: 3},
  {user_id: 15, team_id: 3},
  {user_id: 16, team_id: 3},
  {user_id: 17, team_id: 3}
])
Role.create!([
  {name: "moderator", resource_type: "Team", resource_id: 1},
  {name: "moderator", resource_type: "Team", resource_id: 4},
  {name: "moderator", resource_type: "Team", resource_id: 2},
  {name: "moderator", resource_type: "Team", resource_id: 3}
])

Game.create!([
  {start: "2019-03-19 14:12:22", end: "2019-03-19 14:12:22", home_score: 2, away_score: 0, field: "Field 4", home_team_id: 2, away_team_id: 4},
  {start: "2019-03-12 20:15:00", end: "2019-03-12 20:15:00", home_score: 1, away_score: 0, field: "Field 6", home_team_id: 1, away_team_id: 2},
  {start: "2019-03-05 20:18:00", end: "2019-03-05 20:18:00", home_score: 3, away_score: 1, field: "Field 1", home_team_id: 2, away_team_id: 3},
  {start: "2019-04-19 14:12:22", end: "2019-04-19 14:12:22", home_score: 0, away_score: 0, field: "Field 4", home_team_id: 2, away_team_id: 4},
  {start: "2019-04-12 20:15:00", end: "2019-04-12 20:15:00", home_score: 0, away_score: 0, field: "Field 6", home_team_id: 1, away_team_id: 2},
  {start: "2019-04-05 20:18:00", end: "2019-04-05 20:18:00", home_score: 0, away_score: 0, field: "Field 1", home_team_id: 2, away_team_id: 3}
])
Event.create!([
  {team: "Biggest Ballers", kind: "Yellow Card", name: "Harry Suave", minute: 4, reason: "He told me that I'm ugly", game_id: 1},
  {team: "Biggest Ballers", kind: "Goal", name: "Harry Smith", minute: 10, reason: "", game_id: 1},
  {team: "Biggest Baller", kind: "Goal", name: "Harry Smith", minute: 12, reason: "", game_id: 1},
  {team: "Tiny Ballers", kind: "Red Card", name: "Joe Jonas", minute: 60, reason: "Got upset at loosing and spit on me", game_id: 1},
  {team: "Hey we are here too :)", kind: "Goal", name: "Smith Butterson", minute: 15, reason: "NA", game_id: 3},
  {team: "Biggest Ballers", kind: "Goal", name: "Henry Suave", minute: 18, reason: "NA", game_id: 3},
  {team: "Hey we are here too :)", kind: "Red Card", name: "Smith Butterson", minute: 25, reason: "Hit another player ", game_id: 3},
  {team: "Biggest Ballers", kind: "Goal", name: "Ben", minute: 47, reason: "NA", game_id: 3},
  {team: "Biggest Ballers", kind: "Goal", name: "Ben", minute: 68, reason: "NA", game_id: 3},
  {team: "Keep it 1hunnid :)", kind: "Goal", name: "Darth Vader", minute: 66, reason: "NA", game_id: 2}
])
