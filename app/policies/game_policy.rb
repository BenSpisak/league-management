class GamePolicy
  attr_reader :user, :game

  def initialize(user, game)
    @user = user
    @game = game
  end

  def show?
    user.teams.exists?(game.away_team.id) || user.teams.exists?(game.home_team.id) || user == game.home_team.league.user
  end

  def edit?
    user == game.home_team.league.user
  end

  def update?
    user == game.home_team.league.user
  end

  def destroy?
    user == game.home_team.league.user
  end

end
