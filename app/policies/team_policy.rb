class TeamPolicy
  attr_reader :user, :team

  def initialize(user, team)
    @user = user
    @team = team
  end

  def edit?
    user == team.league.user
  end

  def show?
    user == team.league.user || user.teams.exists?(team.id)
  end

  def detach?
    user == team.league.user || user.has_role?(:moderator, team)
  end

  def makeOwner?
    user == team.league.user || user.has_role?(:moderator, team)
  end

  def removeOwner?
    user == team.league.user || user.has_role?(:moderator, team)
  end

  def update?
    user == team.league.user
  end

  def destroy?
    user == team.league.user
  end

  def invite?
    user == team.league.user || user.has_role?(:moderator, team)
  end

  def leave?
    team.in?(user.teams)
  end

  def inTeam?
    user.teams.exists?(team.id) || user == team.league.user
  end

end
