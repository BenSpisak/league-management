class LeaguePolicy
  attr_reader :user, :league

  def initialize(user, league)
    @user = user
    @league = league
  end

  def show?
    @inLeague = false
    i = 0
    @theseTeams = user.teams
    while i < @theseTeams.length && @inLeague == false
      if(league.teams.includes(@theseTeams[i]))
        @inLeague = true
      end
      i+=1
    end
    user == league.user || @inLeague
  end

  def edit?
    user == league.user
  end

  def update?
    user == league.user
  end

  def destroy?
    user == league.user
  end

  def more?
    user == league.user
  end
end
