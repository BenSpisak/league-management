class TeamsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_team, only: [:show, :edit, :update, :destroy]

  def index
    @teams = current_user.teams.all
  end

  def detach
    @team = Team.find(params[:id])
    authorize @team
    @team = Team.find(params[:id])
    @user = User.find(params[:user_id])
    @team.users.delete(@user)
    redirect_to team_path(@team)
  end

  def makeOwner
    @team = Team.find(params[:id])
    authorize @team
    @team = Team.find(params[:team_id])
    @user = User.find(params[:user_id])
    @user.add_role :moderator, @team
    redirect_to team_path(@team)
  end

  def removeOwner
    @team = Team.find(params[:id])
    authorize @team
    @team = Team.find(params[:team_id])
    @user = User.find(params[:user_id])
    @user.remove_role :moderator, @team
    redirect_to team_path(@team)
  end

  def show
    @team = Team.find(params[:id])
    authorize @team
    @users = @team.users.all
    @home = @team.home.all
    @away = @team.away.all
    @games = @home.or(@away)
    @games = @games.order(:start)
  end

  def edit
    authorize @team
  end

  def update
    authorize @team
    respond_to do |format|
      if @team.update(team_params)
        format.html { redirect_to @team, notice: 'Team was successfully updated.' }
        format.json { render :show, status: :ok, location: @team }
      else
        format.html { render :edit }
        format.json { render json: @team.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    authorize @team
    @team.destroy
    respond_to do |format|
      format.html { redirect_to teams_url, notice: 'Team was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_team
      @team = Team.find(params[:id])
    end

    def team_params
      params.require(:team).permit(:league_id, :name, :wins, :losses, :image)
    end
end
