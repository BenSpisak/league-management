class GamesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_game, only: [:show, :edit, :update, :destroy]

  def new
    @the_leagues = League.where(user: current_user)
    @the_teams = Team.where(league: @the_leagues)

    @game = Game.new
  end

  def show
    authorize @game
    @events = @game.events
  end
  
  def edit
    authorize @game
  end

  def create
    @game = Game.new(game_params)

    respond_to do |format|
      if @game.save
        format.html { redirect_to @game, notice: 'Game was successfully created.' }
        format.json { render :show, status: :created, location: @game }
      else
        format.html { render :new }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    authorize @game
    respond_to do |format|
      if @game.update(game_params)
        format.html { redirect_to @game, notice: 'Game was successfully updated.' }
        format.json { render :show, status: :ok, location: @game }
      else
        format.html { render :edit }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    authorize @game
    @team = @game.home_team
    @game = Game.find(params[:id])
    @game.destroy
    respond_to do |format|
      format.html { redirect_to teams_path(@team), notice: 'Game was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_game
      @game = Game.find(params[:id])
    end

    def game_params
      params.require(:game).permit(:date, :start, :end, :home_score, :away_score, :home_team_id, :field, :away_team_id, events_attributes: Event.attribute_names.map(&:to_sym).push(:_destroy))
    end
end
