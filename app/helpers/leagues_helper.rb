module LeaguesHelper
  def showEdit(current_user, league)
    if policy(league).edit?
      button_to "Edit League", edit_league_path, :class => "btn-mid center-block", :method => :get
    end
  end
end
