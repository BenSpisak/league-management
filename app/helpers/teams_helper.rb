module TeamsHelper
  def showDestroy(current_user, team, user)
    if policy(team).detach?
      link_to '<button class = "btn-small">Remove</button>'.html_safe, detach_team_path(team, user_id: user.id), :data => { :confirm => "Are you sure?" }
    end
  end

  def invite(current_user, team)
    if policy(team).invite?
      link_to '<button class = "btn-mid">Invite a player</button>'.html_safe, new_invite_path(invite: { invitable_id: @team.id, invitable_type: 'Team' } )
    end
  end

  def showLeave(current_user, team)
    if policy(team).leave?
      link_to '<button class = "btn-mid center-block">Leave Team</button>'.html_safe, detach_team_path(team, user_id: current_user.id), :data => { :confirm => "Are you sure?" }
    end
  end

  def showOptions(current_user, team, user)
    if policy(team).makeOwner?
      if user.has_role?(:moderator, @team)
        link_to '<button class = "btn-small">Remove Team Owner</button>'.html_safe, removeOwner_team_path(team_id: @team.id, user_id: user.id), :data => { :confirm => "Are you sure?" }
      else
        link_to '<button class = "btn-small">Make Team Owner</button>'.html_safe, makeOwner_team_path(team_id: @team.id, user_id: user.id), :data => { :confirm => "Are you sure?" }
      end
    end
  end

  def showEdit(current_user, team)
    if policy(team).edit?
      button_to "Edit team", edit_team_path, :class => "btn-mid center-block", :method => :get
    end
  end

  def addGame(current_user, team)
    if policy(team).edit?
      button_to "Add a game", new_game_path, :class => "btn-mid", :method => :get
    end
  end
end
