module GamesHelper
  def editGame(current_user, game)
    if policy(game).edit?
      button_to "Edit Game", edit_game_path, :class => "btn-mid center-block", :method => :get
    end
  end

  def showDelete(current_user, game)
    if policy(game).destroy?
      link_to '<button class = "btn-mid center-block">Delete Game</button>'.html_safe, game, method: :delete, data: { confirm: 'Are you sure?' }
    end
  end
end
