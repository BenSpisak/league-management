json.extract! league, :id, :name, :location, :created_at, :updated_at
json.url league_url(league, format: :json)
