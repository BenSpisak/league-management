class User < ApplicationRecord
  rolify
  include Invitation::User

  mount_uploader :image, ImageUploader

  has_many :leagues, dependent: :destroy

  has_many :memberships, dependent: :destroy
  has_many :teams, through: :memberships
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :confirmable
end
