class League < ApplicationRecord
  belongs_to :user
  has_many :teams, dependent: :destroy
  accepts_nested_attributes_for :teams, allow_destroy: true, reject_if: proc { |att| att['name'].blank? }
  mount_uploader :image, ImageUploader
end
