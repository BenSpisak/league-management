class Team < ApplicationRecord
  resourcify
  has_many :memberships, dependent: :destroy
  has_many :users, through: :memberships
  belongs_to :league
  invitable named_by: :name
  has_many :home, class_name: 'Game', foreign_key: 'home_team_id', dependent: :destroy
  has_many :away, class_name: 'Game', foreign_key: 'away_team_id', dependent: :destroy

  mount_uploader :image, ImageUploader
end
