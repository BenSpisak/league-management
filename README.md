# README

This is the application "Out of Your League" by me, Benjamin Spisak. This is my first Ruby on Rails application that I have worked on and I have learned a lot, and hope to continue to learn!

* Ruby version
Ruby version is 2.6.1

* Configuration
To configure, follow these steps
1. Clone the repo
2. Do "bundle install"
3. Do "rake db:create"
4. Do "rake db:migrate"
5. Do "rake db:seed" if you want to use the pre-generated seeds
6. Do "rails s" to start the server

* This application has several different uses that will each be highlighted here

1. Account creation:
You can create and account by just adding a name, email, password, and optional picure. You can later change this if needed. Since the app is in development, it uses "letter_opener" to "send" you an email to add your account.

2. League creation:
After you create an account (or log into on of the pregenerated ones with the seed), you can create a league. This is a simple process where you can select you team name, location, and an option profile picture. Then, you can create teams from there, each with their own names and profile pictures.
Then you can add players to that team. You can also add one user to the team and then make them the "team owner" which will allow them to add players, delete players, and make other players a league owner. The players need to already have made an account to get added to the team. They can also leave the team at any time.
Then you can add games to the league. You will simply click "add game" and then pick the start time, end time, field number, and two teams that will be playing. Later after the game, you can edit everything but the teams playing the game and then also add the score, who scored, yellow cards, and red cards (with all of their individual details).

3. Team ownership:
As explained earlier, teams can have multiple "team owners" who can add or remove players, while also being able to make other people team owners. That is the only functionality they have though and the league owner will set everything else up.

4. Players:
Out of Your League was made with a player-focused ideology, meaning that it wants to make it as easy for the player as possible. Therefore, a player only has the ability to see their team and their league, while they can't see other people's specific team details in their league. This might be changed later but I did this so that it was a safe as possible of an app. Then, the player can see they past games and long with the score and details of those and the future games that will be played. These are seperated so it is easy to understand and it says both the time and date along with the field so they will know where to go.

If there is anything else that I am missing, please let me know and I would be more than happy to work on it since I love this project and want to keep it going!


